/** Test code to check Interface of Worx Automower display
 *  
 *  The code assumes ST7565/ST7567 command set
 */
#include <SPI.h>

static constexpr int kLCDA0Pin    = 5;
static constexpr int kLCDResetPin = 6;
static constexpr int kLCDCSPin    = 10;
static constexpr int kLedPin      = 13;
static constexpr int kLCDSCKPin   = 14;

void setup() {
  // put your setup code here, to run once:
  pinMode(kLCDA0Pin,     OUTPUT);
  pinMode(kLCDResetPin,  OUTPUT);
  pinMode(kLedPin,       OUTPUT);

  pinMode(kLCDCSPin, OUTPUT);

  digitalWrite(kLCDCSPin, HIGH);

  SPI.setSCK(kLCDSCKPin);
  SPI.begin();
}



void setLED(bool on)
{
  digitalWrite(kLedPin, on ? HIGH : LOW);  
}

void assertReset(bool r)
{
  digitalWrite(kLCDResetPin, r ? LOW : HIGH);
}

void assertA0(bool r)
{
  digitalWrite(kLCDA0Pin, r ? HIGH : LOW);
}

void sendCommand(uint8_t cmd)
{
  digitalWrite(kLCDA0Pin, LOW);  // put your main code here, to run repeatedly:

  digitalWrite(kLCDCSPin, LOW);
  SPI.transfer(cmd);
  digitalWrite(kLCDCSPin, HIGH); 
}

void sendCommand(uint8_t cmd1, uint8_t cmd2)
{
  digitalWrite(kLCDA0Pin, LOW);
  digitalWrite(kLCDCSPin, LOW);
  SPI.transfer(cmd1);
  SPI.transfer(cmd2);
  digitalWrite(kLCDCSPin, HIGH); 
}

void sendData(uint8_t data)
{
  digitalWrite(kLCDA0Pin, HIGH);
  digitalWrite(kLCDCSPin, LOW);
  SPI.transfer(data);
  digitalWrite(kLCDCSPin, HIGH); 
}

void loop() 
{
  setLED(true);

  assertReset(true);
  delayMicroseconds(10);
  assertReset(false);
  delayMicroseconds(10);

  sendCommand(0xA2 | 0x00); /* bias */
  sendCommand(0xA0 | 0x00); /* seg direction normal */
  sendCommand(0xC0 | 0x08); /* com direction reverse */

  sendCommand(0x20 | 3);  /* regulation ratio */
  sendCommand(0x81,  34); /* ev */

  //sendCommand(0xF8,  0);  /* booster level */
  //sendCommand(0xAC | 1, 1);  /* booster level */

  sendCommand(0x28 | 7); /* power control -> enable booster, regulator, follower */

  sendCommand(0xAE | 1); /* display on */

  for(int i = 0; i < 132; ++i)
    sendData((0x1 << (i%8)) | 0x80);

  for(int r = 0; r < 8; ++r)
  {
    sendCommand(0x10 | 0, 0);
    sendCommand(0xB0 | r);
    
    for(int i = 0; i < 8; ++i)
      sendData((0x1 << (i%8)) | 0x80);
  }
  while(true);
}
