EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Worx Automower Keypad"
Date "2022-03-08"
Rev "2"
Comp "(c) 2022 Andreas Messer"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x10 J1
U 1 1 618E0E2F
P 1150 2650
F 0 "J1" H 1068 3267 50  0000 C CNN
F 1 "Conn_01x10" H 1068 3176 50  0000 C CNN
F 2 "Connector_FFC-FPC:Hirose_FH12-10S-0.5SH_1x10-1MP_P0.50mm_Horizontal" H 1150 2650 50  0001 C CNN
F 3 "~" H 1150 2650 50  0001 C CNN
	1    1150 2650
	-1   0    0    -1  
$EndComp
Text Label 1350 2250 0    50   ~ 0
KP1
Text Label 1350 2450 0    50   ~ 0
KP3
Text Label 1350 2550 0    50   ~ 0
KP4
Text Label 1350 2650 0    50   ~ 0
KP5
Text Label 1350 2750 0    50   ~ 0
KP6
Text Label 1350 2850 0    50   ~ 0
KP7
Text Label 1350 3050 0    50   ~ 0
KP9
Text Label 1350 2950 0    50   ~ 0
KP8
Text Label 1350 3150 0    50   ~ 0
KP10
$Comp
L Switch:SW_MEC_5E SW3
U 1 1 618E38BE
P 2300 2650
F 0 "SW3" H 2300 2900 50  0000 C CNN
F 1 "Key 1" H 2300 2700 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 2300 2950 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 2300 2950 50  0001 C CNN
	1    2300 2650
	-1   0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW2
U 1 1 618E4D1B
P 2300 2250
F 0 "SW2" H 2300 2500 50  0000 C CNN
F 1 "Key 2" H 2300 2300 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 2300 2550 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 2300 2550 50  0001 C CNN
	1    2300 2250
	-1   0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW1
U 1 1 618E560C
P 2300 1850
F 0 "SW1" H 2300 2100 50  0000 C CNN
F 1 "Key 3" H 2300 1900 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 2300 2150 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 2300 2150 50  0001 C CNN
	1    2300 1850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW7
U 1 1 618E6D3C
P 3000 2250
F 0 "SW7" H 3000 2500 50  0000 C CNN
F 1 "Key 4" H 3000 2300 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 3000 2550 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3000 2550 50  0001 C CNN
	1    3000 2250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW10
U 1 1 618E733F
P 3700 1850
F 0 "SW10" H 3700 2100 50  0000 C CNN
F 1 "Key 7" H 3700 1900 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 3700 2150 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3700 2150 50  0001 C CNN
	1    3700 1850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW6
U 1 1 618EA370
P 3000 1850
F 0 "SW6" H 3000 2100 50  0000 C CNN
F 1 "Key 5" H 3000 1900 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 3000 2150 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3000 2150 50  0001 C CNN
	1    3000 1850
	-1   0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW17
U 1 1 618EA376
P 4400 3050
F 0 "SW17" H 4400 3300 50  0000 C CNN
F 1 "Key 8" H 4400 3100 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 4400 3350 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 4400 3350 50  0001 C CNN
	1    4400 3050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW13
U 1 1 618EA8D4
P 3700 3050
F 0 "SW13" H 3700 3300 50  0000 C CNN
F 1 "Key 6" H 3700 3100 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 3700 3350 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3700 3350 50  0001 C CNN
	1    3700 3050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW16
U 1 1 618EA8DA
P 4400 2650
F 0 "SW16" H 4400 2900 50  0000 C CNN
F 1 "Key 9" H 4400 2700 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 4400 2950 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 4400 2950 50  0001 C CNN
	1    4400 2650
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW8
U 1 1 618EDCCC
P 3000 2650
F 0 "SW8" H 3000 2900 50  0000 C CNN
F 1 "Key Up" H 3000 2700 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 3000 2950 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3000 2950 50  0001 C CNN
	1    3000 2650
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW11
U 1 1 618EDCD2
P 3700 2250
F 0 "SW11" H 3700 2500 50  0000 C CNN
F 1 "Key Down" H 3700 2300 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 3700 2550 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3700 2550 50  0001 C CNN
	1    3700 2250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW15
U 1 1 618EDCD8
P 4400 2250
F 0 "SW15" H 4400 2500 50  0000 C CNN
F 1 "Key 0" H 4400 2300 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 4400 2550 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 4400 2550 50  0001 C CNN
	1    4400 2250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW9
U 1 1 618EEFC7
P 3000 3050
F 0 "SW9" H 3000 3300 50  0000 C CNN
F 1 "Key Back" H 3000 3100 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 3000 3350 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3000 3350 50  0001 C CNN
	1    3000 3050
	-1   0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW12
U 1 1 618EEFCD
P 3700 2650
F 0 "SW12" H 3700 2900 50  0000 C CNN
F 1 "Key OK" H 3700 2700 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 3700 2950 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3700 2950 50  0001 C CNN
	1    3700 2650
	-1   0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW4
U 1 1 618EEFD3
P 2300 3050
F 0 "SW4" H 2300 3300 50  0000 C CNN
F 1 "Key Home" H 2300 3100 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 2300 3350 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 2300 3350 50  0001 C CNN
	1    2300 3050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW14
U 1 1 618F84F6
P 4400 1850
F 0 "SW14" H 4400 2100 50  0000 C CNN
F 1 "Key Start" H 4400 1900 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 4400 2150 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 4400 2150 50  0001 C CNN
	1    4400 1850
	-1   0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5E SW5
U 1 1 618F9AB0
P 2300 3600
F 0 "SW5" H 2300 3850 50  0000 C CNN
F 1 "Key Power" H 2300 3650 50  0000 C CNN
F 2 "ProjectFootprints:Unkown_Tact_5x5x1.6mm" H 2300 3900 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 2300 3900 50  0001 C CNN
	1    2300 3600
	1    0    0    -1  
$EndComp
Text Label 1900 3500 0    50   ~ 0
KP1
Wire Wire Line
	1900 3500 2050 3500
Text Label 2750 3500 2    50   ~ 0
KP10
Wire Wire Line
	2500 3500 2550 3500
Wire Wire Line
	2500 3600 2550 3600
Wire Wire Line
	2550 3600 2550 3500
Connection ~ 2550 3500
Wire Wire Line
	2550 3500 2750 3500
Wire Wire Line
	2100 3600 2050 3600
Wire Wire Line
	2050 3600 2050 3500
Connection ~ 2050 3500
Wire Wire Line
	2050 3500 2100 3500
Text Label 1350 2350 0    50   ~ 0
KP2
Text Label 1850 2750 0    50   ~ 0
KP8
Text Label 1850 3150 0    50   ~ 0
KP9
Text Label 2650 1450 3    50   ~ 0
KP2
Text Label 3350 1450 3    50   ~ 0
KP3
Text Label 4050 1450 3    50   ~ 0
KP4
Text Label 4750 1450 3    50   ~ 0
KP5
Wire Wire Line
	1850 3150 2050 3150
Wire Wire Line
	2050 3150 2050 3050
Wire Wire Line
	2050 3050 2100 3050
Wire Wire Line
	2050 3050 2050 2950
Wire Wire Line
	2050 2950 2100 2950
Connection ~ 2050 3050
Wire Wire Line
	2500 3050 2550 3050
Wire Wire Line
	2650 2950 2550 2950
Wire Wire Line
	2650 2950 2650 2550
Wire Wire Line
	1850 2750 2050 2750
Wire Wire Line
	2050 3150 2750 3150
Connection ~ 2050 3150
Wire Wire Line
	2050 2750 2050 2650
Wire Wire Line
	2050 2650 2100 2650
Connection ~ 2050 2750
Wire Wire Line
	2050 2750 2750 2750
Wire Wire Line
	2050 2650 2050 2550
Wire Wire Line
	2050 2550 2100 2550
Connection ~ 2050 2650
Wire Wire Line
	2500 2550 2550 2550
Wire Wire Line
	2550 2550 2550 2650
Wire Wire Line
	2550 2650 2500 2650
Wire Wire Line
	2550 2550 2650 2550
Connection ~ 2550 2550
Connection ~ 2650 2550
Wire Wire Line
	2650 2550 2650 2150
Wire Wire Line
	2550 3050 2550 2950
Connection ~ 2550 2950
Wire Wire Line
	2550 2950 2500 2950
Text Label 1850 2350 0    50   ~ 0
KP7
Wire Wire Line
	1850 2350 2050 2350
Wire Wire Line
	2050 2350 2050 2250
Wire Wire Line
	2050 2250 2100 2250
Connection ~ 2050 2350
Wire Wire Line
	2050 2350 2750 2350
Wire Wire Line
	2050 2250 2050 2150
Wire Wire Line
	2050 2150 2100 2150
Connection ~ 2050 2250
Wire Wire Line
	2500 2250 2550 2250
Wire Wire Line
	2550 2250 2550 2150
Wire Wire Line
	2550 2150 2500 2150
Wire Wire Line
	2550 2150 2650 2150
Connection ~ 2550 2150
Connection ~ 2650 2150
Wire Wire Line
	2650 2150 2650 1750
Text Label 1850 1950 0    50   ~ 0
KP6
Wire Wire Line
	1850 1950 2050 1950
Wire Wire Line
	2050 1950 2050 1850
Wire Wire Line
	2050 1850 2100 1850
Connection ~ 2050 1950
Wire Wire Line
	2050 1950 2750 1950
Wire Wire Line
	2050 1850 2050 1750
Wire Wire Line
	2050 1750 2100 1750
Connection ~ 2050 1850
Wire Wire Line
	2500 1850 2550 1850
Wire Wire Line
	2550 1850 2550 1750
Wire Wire Line
	2550 1750 2650 1750
Connection ~ 2650 1750
Wire Wire Line
	2650 1750 2650 1450
Wire Wire Line
	2550 1750 2500 1750
Connection ~ 2550 1750
Wire Wire Line
	2800 2950 2750 2950
Wire Wire Line
	2750 2950 2750 3050
Wire Wire Line
	2750 3050 2800 3050
Wire Wire Line
	2750 3050 2750 3150
Connection ~ 2750 3050
Connection ~ 2750 3150
Wire Wire Line
	2750 3150 3450 3150
Wire Wire Line
	3200 3050 3250 3050
Wire Wire Line
	3250 3050 3250 2950
Wire Wire Line
	3250 2950 3200 2950
Wire Wire Line
	3250 2950 3350 2950
Wire Wire Line
	3350 2950 3350 2550
Connection ~ 3250 2950
Wire Wire Line
	2800 2550 2750 2550
Wire Wire Line
	2750 2550 2750 2650
Connection ~ 2750 2750
Wire Wire Line
	2750 2750 3450 2750
Wire Wire Line
	2750 2650 2800 2650
Connection ~ 2750 2650
Wire Wire Line
	2750 2650 2750 2750
Wire Wire Line
	3200 2650 3250 2650
Wire Wire Line
	3250 2650 3250 2550
Wire Wire Line
	3250 2550 3200 2550
Wire Wire Line
	3250 2550 3350 2550
Connection ~ 3250 2550
Connection ~ 3350 2550
Wire Wire Line
	3350 2550 3350 2150
Wire Wire Line
	2800 2250 2750 2250
Wire Wire Line
	2750 2250 2750 2350
Connection ~ 2750 2350
Wire Wire Line
	2750 2350 3450 2350
Wire Wire Line
	2750 2250 2750 2150
Wire Wire Line
	2750 2150 2800 2150
Connection ~ 2750 2250
Wire Wire Line
	3200 2250 3250 2250
Wire Wire Line
	3250 2250 3250 2150
Wire Wire Line
	3250 2150 3200 2150
Wire Wire Line
	3250 2150 3350 2150
Connection ~ 3250 2150
Connection ~ 3350 2150
Wire Wire Line
	2800 1750 2750 1750
Wire Wire Line
	2750 1750 2750 1850
Connection ~ 2750 1950
Wire Wire Line
	2750 1950 3450 1950
Wire Wire Line
	2800 1850 2750 1850
Connection ~ 2750 1850
Wire Wire Line
	2750 1850 2750 1950
Wire Wire Line
	3350 2150 3350 1750
Wire Wire Line
	3200 1850 3250 1850
Wire Wire Line
	3250 1850 3250 1750
Wire Wire Line
	3250 1750 3200 1750
Wire Wire Line
	3250 1750 3350 1750
Connection ~ 3250 1750
Connection ~ 3350 1750
Wire Wire Line
	3350 1750 3350 1450
Wire Wire Line
	3500 2950 3450 2950
Wire Wire Line
	3450 2950 3450 3050
Connection ~ 3450 3150
Wire Wire Line
	3450 3150 4150 3150
Wire Wire Line
	3500 3050 3450 3050
Connection ~ 3450 3050
Wire Wire Line
	3450 3050 3450 3150
Wire Wire Line
	3900 3050 3950 3050
Wire Wire Line
	3950 3050 3950 2950
Wire Wire Line
	3950 2950 3900 2950
Wire Wire Line
	3950 2950 4050 2950
Wire Wire Line
	4050 2950 4050 2550
Connection ~ 3950 2950
Wire Wire Line
	3500 2550 3450 2550
Wire Wire Line
	3450 2550 3450 2650
Connection ~ 3450 2750
Wire Wire Line
	3450 2750 4150 2750
Wire Wire Line
	3450 2650 3500 2650
Connection ~ 3450 2650
Wire Wire Line
	3450 2650 3450 2750
Wire Wire Line
	3900 2650 3950 2650
Wire Wire Line
	3950 2650 3950 2550
Wire Wire Line
	3950 2550 3900 2550
Wire Wire Line
	3950 2550 4050 2550
Connection ~ 3950 2550
Connection ~ 4050 2550
Wire Wire Line
	4050 2550 4050 2150
Wire Wire Line
	3500 2150 3450 2150
Wire Wire Line
	3450 2150 3450 2250
Connection ~ 3450 2350
Wire Wire Line
	3450 2350 4150 2350
Wire Wire Line
	3450 2250 3500 2250
Connection ~ 3450 2250
Wire Wire Line
	3450 2250 3450 2350
Wire Wire Line
	3900 2250 3950 2250
Wire Wire Line
	3950 2250 3950 2150
Wire Wire Line
	3950 2150 3900 2150
Wire Wire Line
	3950 2150 4050 2150
Connection ~ 3950 2150
Connection ~ 4050 2150
Wire Wire Line
	4050 2150 4050 1750
Wire Wire Line
	3450 1950 3450 1850
Wire Wire Line
	3450 1750 3500 1750
Connection ~ 3450 1950
Wire Wire Line
	3450 1950 4150 1950
Wire Wire Line
	3500 1850 3450 1850
Connection ~ 3450 1850
Wire Wire Line
	3450 1850 3450 1750
Wire Wire Line
	3900 1850 3950 1850
Wire Wire Line
	3950 1850 3950 1750
Wire Wire Line
	3950 1750 3900 1750
Wire Wire Line
	3950 1750 4050 1750
Connection ~ 3950 1750
Connection ~ 4050 1750
Wire Wire Line
	4050 1750 4050 1450
Wire Wire Line
	4200 2950 4150 2950
Wire Wire Line
	4150 2950 4150 3050
Wire Wire Line
	4150 3050 4200 3050
Wire Wire Line
	4150 3150 4150 3050
Connection ~ 4150 3050
Wire Wire Line
	4600 3050 4650 3050
Wire Wire Line
	4650 3050 4650 2950
Wire Wire Line
	4650 2950 4600 2950
Wire Wire Line
	4650 2950 4750 2950
Wire Wire Line
	4750 2950 4750 2550
Connection ~ 4650 2950
Wire Wire Line
	4150 2750 4150 2650
Wire Wire Line
	4150 2650 4200 2650
Wire Wire Line
	4150 2650 4150 2550
Wire Wire Line
	4150 2550 4200 2550
Connection ~ 4150 2650
Wire Wire Line
	4600 2650 4650 2650
Wire Wire Line
	4650 2650 4650 2550
Wire Wire Line
	4650 2550 4600 2550
Wire Wire Line
	4650 2550 4750 2550
Connection ~ 4650 2550
Connection ~ 4750 2550
Wire Wire Line
	4750 2550 4750 2150
Wire Wire Line
	4200 2250 4150 2250
Wire Wire Line
	4150 2250 4150 2350
Wire Wire Line
	4150 2250 4150 2150
Wire Wire Line
	4150 2150 4200 2150
Connection ~ 4150 2250
Wire Wire Line
	4600 2150 4650 2150
Connection ~ 4750 2150
Wire Wire Line
	4750 2150 4750 1750
Wire Wire Line
	4650 2150 4650 2250
Wire Wire Line
	4650 2250 4600 2250
Connection ~ 4650 2150
Wire Wire Line
	4650 2150 4750 2150
Wire Wire Line
	4150 1950 4150 1850
Wire Wire Line
	4150 1850 4200 1850
Wire Wire Line
	4150 1850 4150 1750
Wire Wire Line
	4150 1750 4200 1750
Connection ~ 4150 1850
Wire Wire Line
	4600 1750 4650 1750
Connection ~ 4750 1750
Wire Wire Line
	4750 1750 4750 1450
Wire Wire Line
	4600 1850 4650 1850
Wire Wire Line
	4650 1850 4650 1750
Connection ~ 4650 1750
Wire Wire Line
	4650 1750 4750 1750
$Comp
L Mechanical:Housing N1
U 1 1 61B66293
P 1350 1650
F 0 "N1" H 1503 1684 50  0000 L CNN
F 1 "Housing" H 1503 1593 50  0000 L CNN
F 2 "ProjectFootprints:Worx_WGxxx_Console" H 1400 1700 50  0001 C CNN
F 3 "~" H 1400 1700 50  0001 C CNN
	1    1350 1650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
