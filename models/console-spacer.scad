
height_pcb  = 0.6;
height_tact = 1.6-0.2; // -0.2 shpuld make the caps a bit elevated after mounting
height_cap  = 0.3;

display_pcb = false;


module base() {
  translate([-52.28,-56.95,0]) {
    linear_extrude(height=height_pcb + height_tact + height_cap) {
    
      difference () {
        import("spacer-contour-outer.svg");
        import("spacer-contour-display-cutout.svg");
        import("spacer-contour-button-cutout.svg");
      }
    }
    
    translate([0,0, height_pcb + height_tact])
      linear_extrude(height=height_cap) {
        import("spacer-contour-button-cap.svg");
    }
  }
}


module pcb(kicad=true) {
  translate([0,-(30.3-13.8),0]) // Move SW4 to x,y=0
    translate([0,(56.7-53.72),0]) // Move SW4 to cutout
    {
      /* outline of raw pcb plus some extra space */
      translate([0,0,0.3])
        cube([69,61,0.6],center=true);
      
      /* kicad export of pcb */
      if (kicad)
        import("keyboard-pcb.stl");
    }
}



module spacer() {
  difference () {
    base();      
    pcb(kicad = false );
  }
}

//projection() translate([0,0,20]) 
  spacer();


if (display_pcb) {
  translate([0,0,-10])
    color([0,1.0,0])
      pcb();
}




