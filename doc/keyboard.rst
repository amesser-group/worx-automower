Keyboard Repair
===============

Since the Foil-Keyboard turned out to be unrepairable, I prepared a 
0.6mm thick PCB with some tact switches of 1.6mm height. Unfortunately
I was delivered a different FPC connector than expected by datasheet,
thus I decided to solder a flat cable from former UDMA100 IDE cable
instead.

.. figure:: images/keyboard-pcb.jpg
   :figwidth: 500 px

The pcb was embedded into 3d printed spacer providing caps for the
tact switches and an equal height for the printed foil of the keyboard.

.. figure:: images/keyboard-spacer.jpg
   :figwidth: 500 px

I finally peeled off the printed foild of the original pcb and glued 
it using Spray-Glue onto the the spacer.

.. figure:: images/keyboard-finished.jpg
   :figwidth: 500 px




