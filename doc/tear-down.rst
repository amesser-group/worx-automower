Tear-Down Analysis
==================

Display
-------

Using a small test program with teensy and some labels on the display
cable I found that the display is a 128x64 GLCD with ST7565 / ST7567 
compatible controller.

:Resolution: 128x64
:Active Area: 61.5x25mm
:Controller: ST7565/ST7567 or compatible type

Notes
^^^^^

- Seems not to react on booster / static indicator command
- COM segment direction is reversed

