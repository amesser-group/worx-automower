Display Repair
==============

Due to permanent exposure to sun, the display degraded and half
of the pixels became static dark. Unfortunately, I was not able to find
an exact matching display. It seemed to be custom made, even with pixel 
wider than high. (Rectangles) Thus I need to find a best matching 
replacement.

The display had 128x64 pixels which is a common size of graphics LCDs.
Using my breadboard and a microcontroller I found the original
display to work with a ST7565R similar protocol. Using this information
I choosed a Displaytech 64128K module as replacement part. Its width 
is smaller than the original one but the height fits.

I had to design a new carier PCB to hold the display and it and the 
keyboard with the mainboard of the mower.

.. warning::

   The current PCB uses a 2x5 Standard pin header since I ordered
   the wrong FPC connector. This Pin-Header needs to be replaced
   with a proper FPC connector.

.. figure:: images/display-pcb-front.jpg
   :figwidth: 500 px

.. figure:: images/display-pcb-back.jpg
   :figwidth: 500 px
