#####################
Worx Automower Repair
#####################


Overview
========

Back in 2017 we bought an an Worx Landroid WR111MI automower 
to help my grand parents with their garden. First bigger maintenance
happended during spring 2021 when the rechargeable battery was 
replaced. Unfortunately in the same year the charging station was
blown by some unkown event and thus replaced as well. Besides that,
the foil keyboard and display started to degrade already since 
about 2019/2020 almost failing in the end of 2021. Since we did 
some reasonable maintenance investment already, I decided to give 
it a second chance and replaced keyboard and display.


Work items
==========

Keyboard repair
---------------

Since the auto mower is protected by a PIN code which can not
be disabled it is essential for its operation that the keyboard
works properly. During time, one button after another of the foil 
keyboard stopped its operation. When trying to separate the foils 
from each other to clean contacts I made things even worse and 
broke the keyboard completely. Thus the whole keyboard needed 
to be replaced. Since there was no spare part available and a 
foil keyboard ist not made simply, I replaced the
entire keyboard with a thin PCB with tact switches. Using a 
3d-printed I was able to re-use the printed foil of the original
keyboard.

See `doc/keyboard.rst <doc/keyboard.rst>`_ for more details.


Display repair
--------------

While display and keyboard are protected by a foldable cover, there
is a flaw in its design. The cover has a transparent section
which allows to read the display with closed cover. But this exposes 
the display permanently to the sun. As a result, display will degrade 
with time and become unreadable in affected areas.

Unfortunately, the I was not able to find an exact matching replacement
display. Maybe it was custom made. Thus I need to replace it with one
of matching protocol and comparable size.

See `doc/display.rst <doc/display.rst>`_ for more details.

Status
======

All Repairs done and working fine. 

Licensing
=========

CC BY-SA if not otherwise noted..

Copyright
=========

If not otherwise noted, copyright is
(c) 2021-2022 Andreas Messer <andi@bastelmap.de>. 

References
==========

.. target-notes::
